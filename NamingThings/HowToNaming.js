// Never use one letter variables. Always use meaningful names.
let a = 5; // bad
let count = 5; // good

// Never abbreviate names. Always use full words.
let n = 'John'; // bad
let name = 'John'; // good

// don't put type information in variable names
let sFirstName = 'John'; // bad
let firstName = 'John'; // good

// use units in variable names
let time = 60; // bad
let timeInSeconds = 60; // good